package br.edu.estacio;

import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import com.db4o.cs.Db4oClientServer;

import br.edu.estacio.model.Pessoa;

public class Client {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ObjectContainer client = Db4oClientServer.openClient(Db4oClientServer.newClientConfiguration(), 
				"localhost", 3381, "db4o", "db4o");
		
		ObjectSet<Pessoa> os = client.queryByExample(new Pessoa("Erika"));
		while (os.hasNext()) {
			Pessoa p = os.next();
			System.out.println(p.getNome());
		}
		
		client.close();
		

	}

}
